const seb_api_helpers = require('./../../env/SEBapi.helpers');
const fetch = require('node-fetch');

//auth object, currently only used during SEB auth initialization
class AuthRequest {
    constructor(psu_id,psu_corporate_id,bic ,scopes){
        //generate dummy request ID
        this.psu_id = psu_id;
        this.psu_corporate_id = psu_corporate_id;
        this.bic ="EEUHEE2X";
        this.scopes = "accounts";
        this.status = 'initialized';
    }
    //DEFAULTED oauth SCA method (defaulted to mobileID unless specified otherwise)
    setScaMethod(sca_method){
        this.sca_method = sca_method;
    }
}

module.exports = {
    startAuth: (args) => {
        var authRequest = new AuthRequest(args.psu_id, args.psu_corporate_id,args.bic, args.scopes);
        //SEB API initial payload needed to intitiate decoupled oauth
        var authInitPayload = {
            psu_id: authRequest.psu_id,
            psu_corporate_id: authRequest.psu_corporate_id,
            bic: authRequest.bic,
            //TPP client ID taken from env variables
            client_id: process.env.CLIENT_ID,
            scopes: args.scopes
        }

        //console.log('performing fetch operation');
        return fetch(`https://${process.env.OB_API_HOST}/v2/oauth/authorize-decoupled`,{
            headers: seb_api_helpers.request_headers,
            method: 'POST',
            body: JSON.stringify(authInitPayload),
            agent: seb_api_helpers.api_agent
        })
        .then( res => res.json())
        .then( json => {
            authRequest.authorization_id = json.authorization_id;
            authRequest.status = json.status;
            console.log(json);

            //HARCODED: set SCA auth method on decpupled auth request (make separate mutation or add another mutation input for sca)
            return fetch(`https://${process.env.OB_API_HOST}/v2/oauth/authorize-decoupled/${authRequest.authorization_id}`,{
                headers: seb_api_helpers.request_headers,
                method: 'PATCH',
                body: JSON.stringify({
                    chosen_sca_method: "SmartID"
                }),
                agent: seb_api_helpers.api_agent
            })
            .then( res => res.json())
            .then( json => {
                console.log(json);
                authRequest.chosen_sca_method = json.chosen_sca_method;
                authRequest.challenge_code = json.challenge_data.code;
                //return auth request with challenge code 
                return authRequest; 
            })
            .catch(err =>{
                //return erronous request
                authRequest.status = `Failed : ${err}`;
                return authRequest; 
            });
        })
        .catch(err =>{
            //return erronous request
            authRequest.status = `Failed : ${err}`;
            return authRequest; 
        }); 

               

    },
    authStatus(args){
        return fetch(`https://${process.env.OB_API_HOST}/v2/oauth/authorize-decoupled/${args.authorization_id}`,{
                headers: seb_api_helpers.request_headers,
                method: 'GET',
                agent: seb_api_helpers.api_agent
            })
            .then( res => res.json())
            .then( json => { 
                return json.status; 
            })
            .catch(err =>{
                //return erronous request
                authRequest.status = `Failed : ${err}`;
                return authRequest; 
            });
    },
    generateAuthToken(args){
        return fetch(`https://${process.env.OB_API_HOST}/v2/oauth/token`,{
                headers: seb_api_helpers.request_headers,
                method: 'POST',
                body: JSON.stringify({
                    grant_type: "decoupled_authorization",
                    authorization_id: args.authorization_id
                }),
                agent: seb_api_helpers.api_agent
            })
            .then( res => res.json())
            .then( json => {
                console.log(json);
                return json; 
            }).catch(err =>{
                //return erronous request
                return `Failed : unable to generate token for given authorization id`;
            });
    }
};