const {buildSchema} = require('graphql');

//initial schema consisting of auth object (all fields from SEB) and basic query and mutators
module.exports = buildSchema(`
type Query{
    authStatus(authorization_id: String!): String
}

type Mutation {
    startAuth(psu_id: String!, psu_corporate_id: String!, bic: String = "EEUHEE2X", scopes: String = "accounts"): AuthRequest
    generateAuthToken(authorization_id: String!): Token
}

type AuthRequest{
    psu_id: String!
    psu_corporate_id: String!
    bic: String!
    scopes: [String]!
    authorization_id: String
    status: String
    chosen_sca_method: String
    challenge_code: Int
}
type Token{
    token_type: String
    access_token: String
    expires_in: Int
    refresh_token: String
    refresh_token_expires_in: Int
    scope: String
}
`);