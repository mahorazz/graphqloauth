const https = require('https');
const fs = require('fs');
const moment = require('moment');
moment.locale('en-gb');
var options = {
    cert: fs.readFileSync('env/keys/TPP.crt'),
    key: fs.readFileSync('env/keys/TPP.key'),
    //DEV: test API SSL certificate expiry bypass
    rejectUnauthorized: false  
};
exports.api_agent = https.Agent(options);

exports.request_headers = {
    'x-request-id': 'REQ'+ Math.random().toString(36).substr(10),
    'date': moment().locale('en-gb').format('llll') + ' GMT',
    'Content-Type': 'application/json',
    'accept': 'application/json',
};