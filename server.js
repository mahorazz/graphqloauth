const express = require('express');
const {graphqlHTTP} = require('express-graphql');

const graphQlSchema = require('./graphql/schema/index');
const graphQlResolvers = require('./graphql/resolvers/index');

//initialize the express app and env variables
require('dotenv').config();
const app = express();
app.use(express.json());

//initialize a graphQL server application root
app.use('/graphql', graphqlHTTP({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
}));

app.listen(process.env.PORT, () => {
    console.log(`Oauth QL server running on localhost:${process.env.PORT}`);
});