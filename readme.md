## SEB OB graphQL server
This GraphQL api can make oauth decoupled api requests on the SEB open banking system
**API setup steps**

1.  setup .env variables
2.  place SEB API key and cert files in ./env/keys folder and name them TPP.crt and TPP.key appropriately
3.  npm install
    

# API details

* -- graphQL API endpoint  --
* /graphql
* **bold** - required
* basic mutators (Mutation)
-----------------------------------------------------------------
startAuth(**psu_id**: String, **psu_corporate_id**: String, bic: String, scopes: String)
-----------------------------------------------------------------

**Input Parameters**
```
psu_id              String **required** 
                    Username provided by SEB
psu_corporate_id    String **required** 
                    SEB Account ID, can be either personal or corporate ID
bic                 String *optional*   default = EEUHEE2X
                    Bank natinality ID estonian - EEUHEE2X, latvian - UNLALV2X, lithuanian - CBVILT2X                    
scopes              String *optional*   default = Accounts
                    Scope of the authorization given to the TPP ['accounts payments consents account.lists funds.confirmations']
```
**Return values**
```
{
    psu_id: String
    psu_corporate_id: String
    bic: String
    scopes: [String]
    authorization_id: String
    status: String
    chosen_sca_method: String
    challenge_code: Int
}
```


------------------------------------------------------------------
createAccessToken(**authorization_id**: String)
------------------------------------------------------------------

**Input Parameters**
```
authorization_id    String **required** 
                    Autorization ID provided by SEB to continue auth process (provided during startAuth)            
```
**Return values**
```
{
    token_type: String
    access_token: String
    expires_in: Int
    refresh_token: String
    refresh_token_expires_in: Int
    scope: String
}
```


#Basic queries (Query)
-------------------------------------------------------------------
authStatus(**autorization_id**: String)
-------------------------------------------------------------------
**Input Parameters**
```
authorization_id    String **required** 
                    Autorization ID provided by SEB to continue auth process (provided during startAuth)
```
**Return values**
```
{
    String          Status of the authorization process ("sca method chosen", "finalized")
}
```